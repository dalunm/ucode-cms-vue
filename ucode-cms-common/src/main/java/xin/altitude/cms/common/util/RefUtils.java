package xin.altitude.cms.common.util;

import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.core.toolkit.support.LambdaMeta;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import org.apache.ibatis.reflection.property.PropertyNamer;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author 赛先生和泰先生
 * @author 笔者专题技术博客 —— http://www.altitude.xin
 * @author B站视频 —— https://space.bilibili.com/1936685014
 **/
public class RefUtils {
    private RefUtils() {
    }

    @SuppressWarnings("unchecked")
    public static <T, R> R getFiledValue(T t, SFunction<T, R> action) {
        String fieldName = Optional.ofNullable(getFiledName(action)).orElse("");
        Field field = null;
        try {
            field = t.getClass().getField(fieldName);
            return (R) field.get(t);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 通过反射 给属性赋值
     *
     * @param t
     * @param action
     * @param value
     * @param <S>
     * @param <RR>
     */
    public static <T, S, RR> void setFiledValue(T t, SFunction<S, RR> action, Object value) {
        String fieldName = Optional.ofNullable(getFiledName(action)).orElse("");
        try {
            Field field = t.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(t, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static <T> void setFiledValue(T t, String fieldName, Object value) {
        try {
            Field field = t.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(t, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    /**
     * 通过方法引用获取指定实体类的字段名（属性名）
     *
     * @param action
     * @param <T>
     * @param <R>
     * @return
     */
    public static <T, R> String getFiledName(SFunction<T, R> action) {
        return Optional.ofNullable(action).map(LambdaUtils::extract).map(LambdaMeta::getImplMethodName).map(PropertyNamer::methodToProperty).orElse(null);
    }

    /**
     * 通过方法引用获取指定实体类的字段名（属性名）
     *
     * @param <T>
     * @param <R>
     * @param action
     * @return
     */
    @SafeVarargs
    public static <T, R> List<String> getFiledNames(SFunction<T, R>... action) {
        return Arrays.stream(action).map(LambdaUtils::extract)
            .map(LambdaMeta::getImplMethodName)
            .map(PropertyNamer::methodToProperty)
            .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public static <T> T getFieldValue(Object entity, String fieldName) {
        return (T) ReflectionKit.getFieldValue(entity, fieldName);
    }


    /**
     * 通过Clazz对象创建实例
     *
     * @param clazz CLass对象
     * @param <T>   泛型
     * @return 泛型实例
     */
    public static <T> T newInstance(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}
