/*
 *
 * Copyright (c) 2020-2022, Java知识图谱 (http://www.altitude.xin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package xin.altitude.cms.common.util;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author <a href="http://www.altitude.xin" target="_blank">Java知识图谱</a>
 * @author <a href="https://gitee.com/decsa/ucode-cms-vue" target="_blank">UCode CMS</a>
 * @author <a href="https://space.bilibili.com/1936685014" target="_blank">B站视频</a>
 **/
public class FieldInjectUtils {
    private FieldInjectUtils() {
    }

    /**
     * 通过反射的方式给关联表查询注入属性值
     * 需要说明的是 相较于手动编码 反射的执行效率略微差点
     * 本方法优点是能够提高开发效率
     * 后续考虑逆向工程优化性能
     *
     * @param <T>           主表对应的实体类VO
     * @param <S>           副表对应的实体类DO
     * @param <R>           主表外键副表主键对应的数据类型
     * @param <W>           需要注入属性的列泛型
     * @param data          主表对应的VO集合实例
     * @param fkColumn      主表关联外键列（字段） 方法引用表示
     * @param clazz         副表对应的IService实现类Class对象
     * @param pkColumn      副表对应的主键（字段）方法引用表示
     * @param injectColumns 需要注入的列（字段） 方法引用表示
     */
    @SafeVarargs
    public static <T, S, R, W> void injectField(List<T> data, final SFunction<T, R> fkColumn, Class<? extends IService<S>> clazz, final SFunction<S, R> pkColumn, SFunction<T, W>... injectColumns) {
        IService<S> iService = SpringUtils.getBean(clazz);
        // 获取主表关联外键的集合（去重）
        Set<? extends R> ids = EntityUtils.toSet(data, fkColumn);
        // 如果集合元素个数不为空 则进行后续操作
        if (ids.size() > 0 && injectColumns.length > 0) {
            // 主表关联外键对应的字段名字符串
            String fieldName = RefUtils.getFiledName(fkColumn);
            List<String> injectFiledNames = RefUtils.getFiledNames(injectColumns);
            String[] selectField = {fieldName, String.join(",", injectFiledNames)};
            // 数据库查询字段需要下划线表示
            String selectStr = Arrays.stream(selectField).map(StringUtil::toUnderScoreCase).collect(Collectors.joining(","));
            // 构造副表查询条件(查询指定列元素)
            QueryWrapper<S> wrapper = Wrappers.query(RefUtils.newInstance(iService.getEntityClass())).select(selectStr).in(StringUtil.toUnderScoreCase(fieldName), ids);
            // 通过主表的外键查询关联副表符合条件的数据
            List<S> list = iService.getBaseMapper().selectList(wrapper);
            // 将list转换为map 其中Key为副表主键 Value为副表类型实例本身
            Map<R, S> map = EntityUtils.toMap(list, pkColumn, e -> e);
            // if (fieldName != null && data.size() > 0) {
            //     BeanCopier beanCopier = BeanCopier.create(iService.getEntityClass(), ColUtils.toObj(data).getClass(), false);
            //     for (T t : data) {
            //         // 获取当前主表VO对象实例关联外键的值
            //         R r = RefUtils.getFieldValue(t, fieldName);
            //         // 从map中取出关联副表的对象实例
            //         S s = map.get(r);
            //         // 将副表中指定属性的值 按照同名的原则 复制到 主表VO中
            //         beanCopier.copy(s, t, null);
            //     }
            // }
            doInjectField(data, fieldName, map, RefUtils.getFiledNames(injectColumns));
        }
    }

    /**
     * 通过反射的方式给关联表查询注入属性值
     * 需要说明的是 相较于手动编码 反射的执行效率略差
     * 本方法能够提高开发效率 后续考虑逆向工程优化性能
     *
     * @param <T>           主表对应的实体类VO
     * @param <S>           副表对应的实体类DO
     * @param <R>           主表外键副表主键对应的数据类型
     * @param <W>           需要注入属性的列泛型
     * @param data          主表对应的VO集合实例
     * @param fkColumn      主表关联外键列（字段） 方法引用表示
     * @param subData       副表对应DO集合实例
     * @param pkColumn      副表对应的主键（字段）方法引用表示
     * @param injectColumns 需要注入的列（字段） 方法引用表示
     */
    @SafeVarargs
    public static <T, S, R, W> void injectField(List<T> data, SFunction<T, R> fkColumn, List<S> subData, SFunction<S, R> pkColumn, SFunction<T, W>... injectColumns) {
        // 如果集合元素个数不为空 则进行后续操作
        if (subData.size() > 0 && injectColumns.length > 0) {
            // 主表关联外键对应的字段名字符串
            String fieldName = RefUtils.getFiledName(fkColumn);
            // 将list转换为map 其中Key为副表主键 Value为副表类型实例本身
            Map<R, S> map = EntityUtils.toMap(subData, pkColumn, e -> e);
            doInjectField(data, fieldName, map, RefUtils.getFiledNames(injectColumns));
        }
    }

    @SafeVarargs
    public static <T, S, R, W> void injectField2(List<T> data, final SFunction<T, R> fkColumn, Class<? extends IService<S>> clazz, final SFunction<S, R> pkColumn, SFunction<T, W>... injectColumns) {
        IService<S> iService = SpringUtils.getBean(clazz);
        // 获取主表关联外键的集合（去重）
        Set<? extends R> ids = EntityUtils.toSet(data, fkColumn);
        // 如果集合元素个数不为空 则进行后续操作
        if (ids.size() > 0 && injectColumns.length > 0) {
            // 主表关联外键对应的字段名字符串
            String fieldName = RefUtils.getFiledName(fkColumn);
            // 构造副表查询条件(查询整行元素)
            LambdaQueryWrapper<S> wrapper = Wrappers.lambdaQuery(iService.getEntityClass()).in(pkColumn, ids);
            // 通过主表的外键查询关联副表符合条件的数据
            List<S> list = iService.getBaseMapper().selectList(wrapper);
            // 将list转换为map 其中Key为副表主键 Value为副表类型实例本身
            Map<R, S> map = EntityUtils.toMap(list, pkColumn, e -> e);
            doInjectField(data, fieldName, map, RefUtils.getFiledNames(injectColumns));
        }
    }


    /**
     * 具体执行属性复制逻辑 原理使用反射 确保仅复制指定字段 严格保证数据正确性
     *
     * @param data       含有空值属性的对象集合
     * @param fieldName  主表关联外键字段名
     * @param map        副表主键与当前对象Map
     * @param filedNames 需要注入属性的字段（驼峰表示）
     * @param <T>        主表VO泛型
     * @param <S>        副表DO泛型
     * @param <R>        主表关联外键、副表主键的类型泛型
     * @param <W>        需要注入列泛型（不同列可能类型不同）
     */
    private static <T, S, R, W> void doInjectField(List<T> data, String fieldName, Map<R, S> map, List<String> filedNames) {
        if (fieldName != null && filedNames.size() > 0) {
            for (T t : data) {
                // 获取当前主表VO对象实例关联外键的值
                R r = RefUtils.getFieldValue(t, fieldName);
                // 从map中取出关联副表的对象实例
                S s = map.get(r);
                // 使用Spring内置的属性复制方法
                BeanCopyUtils.copyProperties(s, t, filedNames);
            }
        }
    }

    /**
     * 通过反射的方式给关联表查询注入属性值
     * 需要说明的是 相较于手动编码 反射的执行效率略差
     * 本方法能够提高开发效率 后续考虑逆向工程优化性能
     *
     * @param page          主表对应的VO分页实例
     * @param fkColumn      主表关联外键列（字段） 方法引用表示
     * @param clazz         副表对应的IService实例
     * @param pkColumn      副表对应的主键（字段）方法引用表示
     * @param injectColumns 需要注入的列（字段） 方法引用表示
     * @param <T>           主表对应的实体类VO
     * @param <S>           副表对应的实体类DO
     * @param <R>           主表外键副表主键对应的数据类型
     * @param <W>           需要注入属性的列泛型
     */
    @SafeVarargs
    public static <T, S, R, W> void injectField(IPage<T> page, final SFunction<T, R> fkColumn, Class<? extends IService<S>> clazz, final SFunction<S, R> pkColumn, SFunction<T, W>... injectColumns) {
        List<T> records = page.getRecords();
        injectField(records, fkColumn, clazz, pkColumn, injectColumns);
    }
}
